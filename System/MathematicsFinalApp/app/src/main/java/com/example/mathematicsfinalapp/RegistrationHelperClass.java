package com.example.mathematicsfinalapp;

public class RegistrationHelperClass {
    String email, name, idNumber, address, password;

    public RegistrationHelperClass() {
    }

    public RegistrationHelperClass(String email, String name, String idNumber, String address, String password) {
        this.email = email;
        this.name = name;
        this.idNumber = idNumber;
        this.address = address;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
