package com.example.mathematicsfinalapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button RegistrationTeacher, RegistrationStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RegistrationTeacher = findViewById(R.id.RegisterTeacher1);
        RegistrationStudent = findViewById(R.id.RegisterStudent1);

        RegistrationTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Activity_login.class);
                startActivity(intent);
            }
        });

        RegistrationStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, login_Student.class);
                startActivity(intent);
            }
        });
    }
}