package com.example.mathematicsfinalapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Registration_Activity extends AppCompatActivity {
    Button register, already;
    TextInputLayout editEmail, editName, editIdNumber, editAddress, editPassword, editRetype;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_);

        register = findViewById(R.id.registerUser);
        already = findViewById(R.id.haveAccount);
        editEmail = findViewById(R.id.email);
        editName = findViewById(R.id.name);
        editIdNumber = findViewById(R.id.idNumber);
        editAddress = findViewById(R.id.address);
        editPassword = findViewById(R.id.password);
        editRetype = findViewById(R.id.rePassword);

        already.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registration_Activity.this, Activity_login.class);
                startActivity(intent);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass = editPassword.getEditText().toString();
                String repass = editRetype.getEditText().getText().toString();


                    insertUser();
                    Intent intent = new Intent(Registration_Activity.this, Activity_login.class);
                    startActivity(intent);



            }
        });
    }

    private void insertUser() {
        String email = editEmail.getEditText().getText().toString();
        String name = editName.getEditText().getText().toString();
        String address = editAddress.getEditText().getText().toString();
        String idNumber = editIdNumber.getEditText().getText().toString();
        String password = editPassword.getEditText().getText().toString();

        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("Registration");

        Query checkUser = reference.orderByChild("idNumber").equalTo(idNumber);

        checkUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Toast.makeText(Registration_Activity.this, "User Already Exist", Toast.LENGTH_SHORT).show();
                }
                else {
                    RegistrationHelperClass helperClass = new RegistrationHelperClass(email, name, idNumber, address, password);
                    reference.child(idNumber).setValue(helperClass);
                    Toast.makeText(Registration_Activity.this, "Registered", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}