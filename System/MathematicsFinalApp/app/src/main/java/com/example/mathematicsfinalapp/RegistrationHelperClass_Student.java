package com.example.mathematicsfinalapp;

public class RegistrationHelperClass_Student {

    String email_Student, name_Student, idNumber_Student, address_Student, password_Student;

    public RegistrationHelperClass_Student() {
    }

    public RegistrationHelperClass_Student(String email_Student, String name_Student, String idNumber_Student, String address_Student, String password_Student) {
        this.email_Student = email_Student;
        this.name_Student = name_Student;
        this.idNumber_Student = idNumber_Student;
        this.address_Student = address_Student;
        this.password_Student = password_Student;
    }

    public String getEmail_Student() {
        return email_Student;
    }

    public void setEmail_Student(String email_Student) {
        this.email_Student = email_Student;
    }

    public String getName_Student() {
        return name_Student;
    }

    public void setName_Student(String name_Student) {
        this.name_Student = name_Student;
    }

    public String getIdNumber_Student() {
        return idNumber_Student;
    }

    public void setIdNumber_Student(String idNumber_Student) {
        this.idNumber_Student = idNumber_Student;
    }

    public String getAddress_Student() {
        return address_Student;
    }

    public void setAddress_Student(String address_Student) {
        this.address_Student = address_Student;
    }

    public String getPassword_Student() {
        return password_Student;
    }

    public void setPassword_Student(String password_Student) {
        this.password_Student = password_Student;
    }
}
