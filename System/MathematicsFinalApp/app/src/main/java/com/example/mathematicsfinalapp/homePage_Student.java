package com.example.mathematicsfinalapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class homePage_Student extends AppCompatActivity {
    Button DashboardHome_students;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page__student);

        DashboardHome_students = findViewById(R.id.DashboardHome_Student);

        DashboardHome_students.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homePage_Student.this, Dashboard_Student.class);
                startActivity(intent);
            }
        });
    }
}