package com.example.mathematicsfinalapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Student_Registration extends AppCompatActivity {

    Button register_Student, already_Student;
    TextInputLayout editEmail_Student, editName_Student, editIdNumber_Student, editAddress_Student, editPassword_Student, editRetype_Student;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student__registration);

        register_Student = findViewById(R.id.registerUser_Student);
        already_Student = findViewById(R.id.haveAccount_Student);
        editEmail_Student = findViewById(R.id.email_Student);
        editName_Student = findViewById(R.id.name_Student);
        editIdNumber_Student = findViewById(R.id.idNumber_Student);
        editAddress_Student = findViewById(R.id.address_Student);
        editPassword_Student = findViewById(R.id.password_Student);
        editRetype_Student = findViewById(R.id.rePassword_Student);

        already_Student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Student_Registration.this, login_Student.class);
                startActivity(intent);
            }
        });

        register_Student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass = editPassword_Student.getEditText().toString();
                String repass = editRetype_Student.getEditText().getText().toString();


                insertUser();
                Intent intent = new Intent(Student_Registration.this, login_Student.class);
                startActivity(intent);



            }
        });
    }

    private void insertUser() {
        String email_Student = editEmail_Student.getEditText().getText().toString();
        String name_Student = editName_Student.getEditText().getText().toString();
        String address_Student = editAddress_Student.getEditText().getText().toString();
        String idNumber_Student = editIdNumber_Student.getEditText().getText().toString();
        String password_Student = editPassword_Student.getEditText().getText().toString();

        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("Registration");

        Query checkUser = reference.orderByChild("idNumber").equalTo(idNumber_Student);

        checkUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    Toast.makeText(Student_Registration.this, "User Already Exist", Toast.LENGTH_SHORT).show();
                }
                else {
                    RegistrationHelperClass_Student helperClass = new RegistrationHelperClass_Student(email_Student, name_Student, idNumber_Student, address_Student, password_Student);
                    reference.child(idNumber_Student).setValue(helperClass);
                    Toast.makeText(Student_Registration.this, "Registered", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

}