package com.example.mathematicsfinalapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Activity_login extends AppCompatActivity {
    Button register, login;
    TextInputLayout idNumber, password;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        register = findViewById(R.id.newUser);
        login = findViewById(R.id.loginUser);
        idNumber = findViewById(R.id.loginID);
        password = findViewById(R.id.loginPassword);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_login.this, Registration_Activity.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });


    }

    private void loginUser() {
        String loginID = idNumber.getEditText().getText().toString();
        String Password = password.getEditText().getText().toString();

        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("Registration");

        Query checker = reference.orderByChild("idNumber").equalTo(loginID);

        checker.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){

                    String passwordFromDB = snapshot.child(loginID).child("password").getValue(String.class);

                    if (passwordFromDB.equals(Password)){

                        String nameFromDB = snapshot.child(loginID).child("name").getValue(String.class);
                        String idNumberFromDB = snapshot.child(loginID).child("idNumber").getValue(String.class);
                        String emailFromDB = snapshot.child(loginID).child("email").getValue(String.class);
                        String addressFromDB = snapshot.child(loginID).child("address").getValue(String.class);

                        Intent intent = new Intent(Activity_login.this, Drawer.class);
                        intent.putExtra("name", nameFromDB);
                        intent.putExtra("idNumber", idNumberFromDB);
                        intent.putExtra("email", emailFromDB);
                        intent.putExtra("address", addressFromDB);
                        startActivity(intent);
                    }
                    else {
                        password.setError("Wrong Password");
                        password.requestFocus();
                    }

                }
                else {
                    idNumber.setError("No user Exist");
                    idNumber.requestFocus();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }
}