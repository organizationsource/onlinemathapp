package com.example.mathematicsfinalapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.auth.FirebaseAuthCredentialsProvider;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class DashboardActivity extends AppCompatActivity {
    TextView name, id;
    ImageView profileImage;
    Button logout;
    StorageReference storageReference_teachers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        name = findViewById(R.id.name);
        id = findViewById(R.id.dashID);

        Intent intent = getIntent();
        String getName = intent.getStringExtra("name");
        String getId = intent.getStringExtra("idNumber");

        profileImage = findViewById(R.id.profileImage);

        logout = findViewById(R.id.logout);

        storageReference_teachers = FirebaseStorage.getInstance().getReference();

        name.setText(getName);
        id.setText(getId);


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, Activity_login.class);
                startActivity(intent);
            }
        });








    }


}