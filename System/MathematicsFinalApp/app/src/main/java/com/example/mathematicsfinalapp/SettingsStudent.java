package com.example.mathematicsfinalapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.View;

public class SettingsStudent extends AppCompatActivity {
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_student);

        drawerLayout = findViewById(R.id.drawer_layout);
    }

    public void ClickMenu(View view) {
        //Open drawer
        Drawer.openDrawer(drawerLayout);
    }

    public void ClickLogo(View view) {
        //Close drawer
        Drawer.closeDrawer(drawerLayout);
    }

    public void ClickHome(View view) {
        //Redirect activity to home
        Drawer.redirectActivity(this, StudentDrawer.class);
    }

    public void ClickDashboard(View view) {
        //Redirect activity to dashboard
        Drawer.redirectActivity(this, DashboardStudent.class);
    }

    public void ClickAboutUs(View view) {
        Drawer.redirectActivity(this, AboutUsStudent.class);
    }

    public void ClickSettings(View view) {
        //Recreate activity
        recreate();
    }

    public void ClickLogout(View view) {
        //Close app
        Drawer.logout(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Close drawer
        Drawer.closeDrawer(drawerLayout);

    }
}