 package com.example.mathematicsfinalapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

public class Dashboard_Student extends AppCompatActivity {
    TextView name_Students, id_Students;
    private ImageView profileImage_Students;
    Button logout_Student;
    Button home_student;
    StorageReference storageReference_Students;
    private Uri imageUri;
    private FirebaseStorage storage_Student;
    private StorageReference getStorageReference_Student;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard__student);


        name_Students = findViewById(R.id.name_Students);
        id_Students = findViewById(R.id.dashID_Students);
        logout_Student = findViewById(R.id.logout_Students);
        home_student = findViewById(R.id.home_Students);


        Intent intent = getIntent();
        String getName = intent.getStringExtra("name_Student");
        String getId = intent.getStringExtra("idNumber_Student");

        profileImage_Students = findViewById(R.id.profileImage_Students);
        storage_Student = FirebaseStorage.getInstance();
        storageReference_Students = storage_Student.getReference();



        storageReference_Students = FirebaseStorage.getInstance().getReference();

        name_Students.setText(getName);
        id_Students.setText(getId);


        home_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard_Student.this, homePage_Student.class);
                startActivity(intent);
            }
        });


        logout_Student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard_Student.this, login_Student.class);
                startActivity(intent);
            }
        });


        profileImage_Students.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePicture();

            }
        });


    }


    private void choosePicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 1);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK && data!=null && data.getData()!=null){
            imageUri = data.getData();
            profileImage_Students.setImageURI(imageUri);
            uploadPicture_Student();
        }
    }

    private void uploadPicture_Student() {
       final ProgressDialog pd = new ProgressDialog(this);
       pd.setTitle("Upload Image....");
       pd.show();

       final String randomKey = UUID.randomUUID().toString();
       StorageReference riversRef = storageReference_Students.child("images/"+ randomKey);

       riversRef.putFile(imageUri)
               .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       Snackbar.make(findViewById(android.R.id.content), "Image uploaded.", Snackbar.LENGTH_LONG).show();
                   }
               })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Failed to Upload", Toast.LENGTH_LONG).show();
                    }
                });

    }

}

