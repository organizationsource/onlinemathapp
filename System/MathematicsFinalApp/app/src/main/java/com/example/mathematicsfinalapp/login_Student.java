package com.example.mathematicsfinalapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class login_Student extends AppCompatActivity {
    Button register_Students, login_Students;
    TextInputLayout idNumber_Students, password_Students;
    FirebaseDatabase rootNode_Students;
    DatabaseReference reference_Students;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__student);

        register_Students = findViewById(R.id.newUser_Students);
        login_Students = findViewById(R.id.loginUser_Students);
        idNumber_Students = findViewById(R.id.loginID_Students);
        password_Students = findViewById(R.id.loginPassword_Students);

        register_Students.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(login_Student.this,Student_Registration.class);
                startActivity(intent);
            }
        });


        login_Students.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser_Students();
            }
        });


    }

    private void loginUser_Students() {
        String loginID_Students = idNumber_Students.getEditText().getText().toString().trim();
        String Password_Students = password_Students.getEditText().getText().toString().trim();

        rootNode_Students = FirebaseDatabase.getInstance();
        reference_Students = rootNode_Students.getReference("Registration");

        Query checker = reference_Students.orderByChild("idNumber_Student").equalTo(loginID_Students);

        checker.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){

                    String passwordFromDB_Students = snapshot.child(loginID_Students).child("password_Student").getValue(String.class);

                    if (passwordFromDB_Students.equals(Password_Students)){

                        String nameFromDB_Students = snapshot.child(loginID_Students).child("name_Student").getValue(String.class);
                        String idNumberFromDB_Students = snapshot.child(loginID_Students).child("idNumber_Student").getValue(String.class);
                        String emailFromDB_Students = snapshot.child(loginID_Students).child("email_Student").getValue(String.class);
                        String addressFromDB_Students = snapshot.child(loginID_Students).child("address_Student").getValue(String.class);

                        Intent intent = new Intent(login_Student.this, StudentDrawer.class);
                        intent.putExtra("name_Student", nameFromDB_Students);
                        intent.putExtra("idNumber_Student", idNumberFromDB_Students);
                        intent.putExtra("email_Student", emailFromDB_Students);
                        intent.putExtra("address_Student", addressFromDB_Students);
                        startActivity(intent);
                    }
                    else {
                        password_Students.setError("Wrong Password");
                        password_Students.requestFocus();
                    }

                }
                else {
                    idNumber_Students.setError("No user Exist");
                    idNumber_Students.requestFocus();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }
}